import React, { useState, useLayoutEffect } from "react";
import "./styles.css";

import Tree from "./Tree/Tree";

const structure = [
  
  {
    type: "folder",
    name: "Documents",
    files: [
        { type: "file", name: "Document1.jpg" },
          { type: "file", name: "Document2.jpg" },
          { type: "file", name: "Document3.jpg" },
    ],
  },
  {
    type: "folder",
    name: "Desktop",
    files: [
        { type: "file", name: "Screenshot1.jpg" },
          { type: "file", name: "videopal.mp4" },
    ],
  },
  {
    type: "folder",
    name: "Downloads",
    files: [
        { type: "folder",
         name: "Drivers",
         files:[
          { type: "file", name: "Screenshot1.jpg" },
          { type: "file", name: "videopal.mp4" },
         ],
       },   
    ],
  },
  {
    type: "folder",
    name: "Applications",
    files: [
        { type: "file", name: "Webstorm.dmg" },
          { type: "file", name: "Pycharm.dmg" },
          { type: "file", name: "FileZila.dmg" },
          { type: "file", name: "Mattermost.dmg" },
    ],
  },
  { type: "file", name: "Chromedriver.dmg" },
];

export default function App() {
  let [data, setData] = useState(structure);

  const handleClick = (node) => {
    console.log(node);
  };
  const handleUpdate = (state) => {
    localStorage.setItem(
      "tree",
      JSON.stringify(state, function (key, value) {
        if (key === "parentNode" || key === "id") {
          return null;
        }
        return value;
      })
    );
  };

  useLayoutEffect(() => {
    try {
      let savedStructure = JSON.parse(localStorage.getItem("tree"));
      if (savedStructure) {
        setData(savedStructure);
      }
    } catch (err) {
      console.log(err);
    }
  }, []);

  return (
    <div className="App">
      <div className="container">
      <h2>Evaluation</h2>
      <Tree data={data} onUpdate={handleUpdate} onNodeClick={handleClick} />
      </div>
    </div>
  );
}
