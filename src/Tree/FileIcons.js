import React from "react";
import { DiJavascript1, DiCss3Full, DiHtml5, DiReact } from "react-icons/di";
import {AiOutlineFileJpg,AiFillPlayCircle} from "react-icons/ai"
import { HiMenuAlt2 } from "react-icons/hi";
const FILE_ICONS = {
  dmg: <HiMenuAlt2 />,
  jpg: <AiOutlineFileJpg />,
  mp4: <AiFillPlayCircle />,
  jsx: <DiReact />
};

export default FILE_ICONS;
