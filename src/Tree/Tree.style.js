import styled from "styled-components/macro";

export const StyledTree = styled.div`

  width:94%;
  margin:auto;
  line-height: 1.3;
  z-index:1;
  background:#282828;
  padding:10px 5px;
  border:1px solid rgba(255,255,255,0.2);
  
  .tree__input {
    width: auto;
  }
`;

export const ActionsWrapper = styled.div`
width:100%;
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  justify-content: space-between;
  

  .actions {
    display: flex;
    align-items: center;
    flex-wrap: nowrap;
    justify-content: space-between;
    opacity: 0;
    pointer-events: none;
    transition: 0.2s;
    color:white;

    > svg {
      cursor: pointer;
      margin-left: 10px;
      transform: scale(1);
      transition: 0.2s;

      :hover {
        transform: scale(1.1);
      }
    }
  }

  &:hover .actions {
    opacity: 1;
    pointer-events: all;
    transition: 0.2s;
  }
`;

export const StyledName = styled.div`
  color: rgb(198, 198, 198);
  display: flex;
  align-items: center;
  cursor: pointer;
  padding:5px
`;

export const Collapse = styled.div`
  height: max-content;
  max-height: ${p => (p.isOpen ? "800px" : "0px")};
  overflow: hidden;
  transition: 0.3s ease-in-out;
`;

export const VerticalLine = styled.section`
  position: relative;
  :before {
    content: "";
    display: block;
    position: absolute;
    top: -2px; /* just to hide 1px peek */
    left: 1px;
    width: 0;
    height: 100%;
    border: 0px solid #dbdbdd;
    z-index: -1;
  }
`;
